begin
  retries ||= 0
  puts "try ##{ retries }"
  raise "the roof"
rescue
  retry if (retries += 1) < 3
end

(0..5).each do |i|
  puts "Value: #{i}"
  redo if i > 2
end

(0..5).each do |i|
  puts "Value: #{i}"
  retry if i > 2
end