def total(prices)
  amount = 0
  do_something_with_every_item(prices) do |price|
    amount += price
  end
  return amount
end
 
def refund(prices)
  amount = 0
  do_something_with_every_item(prices) do |price|
    amount -= price
  end
  return amount
end
 
def show_discounts(prices)
  do_something_with_every_item(prices) do |price|
    amount_off = price / 3
    puts "Your discount: $#{amount_off}"
  end
end
 
order = [3, 24, 6]
puts total(order)
puts refund(order)
show_discounts(order)